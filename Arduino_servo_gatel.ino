#include <TimerPWM.h>
#include <PID_v1.h>
#include <FreqMeasure.h>

//pin map
#define VRM 5 //green
//#define SPEED 8 //red
#define BRAKE 2 //white
#define DIRECTION 4 //gray

#define CW  true
#define CCW false

PID *pid;
double duty_cycle=0, pos;
unsigned int buf;
int multiplier;

double sum=0;
int count=0;
int rpm=0;
float frequency=0;
double theta, time_ms;
unsigned long prev_time;
long x,y;
void setup() {
  Serial.begin(9600);
  
  pinMode(13, OUTPUT);
  pinMode(BRAKE, OUTPUT);
  pinMode(DIRECTION, OUTPUT);
  
  digitalWrite(13, HIGH);
  digitalWrite(BRAKE, LOW);
  digitalWrite(DIRECTION, LOW);
  
  pid = new PID(&theta, &duty_cycle, &pos, 0.001, 0, 0, DIRECT);
  pid->SetMode(AUTOMATIC);
  pid->SetOutputLimits(-1023,1023);
  pos = 90;
  pid->SetSampleTime(0);
  if (pos < 0) multiplier = -1;
  else multiplier = 1;
  
  Tpwm.initialize(0, 5);
  Tpwm.pwm(0, VRM, map(labs(duty_cycle), -1023, 1023, 0, 1023));
  FreqMeasure.begin();
  prev_time=micros();
  x=0;
}

void loop() {
  for (x=5;x<=100;x=x+5)
  {
    Tpwm.disablePwm(0,VRM);
    Tpwm.stop(0);
    Tpwm.setPeriod(0, x);
    Tpwm.pwm(0, VRM, map(labs(duty_cycle), -1023, 1023, 0, 1023));
    Tpwm.start(0);
    for(y=0;y<100;y++)
    {
       Tpwm.setPwmDuty(0,VRM,127);
       readSpeed();
       Serial.print(x);
       Serial.print('\t');
       Serial.println(frequency);
//       digitalWrite(13,HIGH);
//       delay(50);
//       digitalWrite(13,LOW);
//       delay(50);
    }
  }
}

void controll()
{
  if (pid->Compute()) {
    if (duty_cycle < 0) {
      digitalWrite(DIRECTION, CW);
      multiplier = -1;
    }
    else {
      digitalWrite(DIRECTION, CCW);
      multiplier = 1;
    }
    Tpwm.setPwmDuty(0, VRM, map(labs(duty_cycle), -1023, 1023, 0, 1023));
  }
}

void readSpeed()
{
  if (FreqMeasure.available())
  {
    sum = sum + FreqMeasure.read();
    count = count + 1;
    if (count >30)
    {
       frequency = FreqMeasure.countToFrequency(sum/count);
       sum=0;
       count=0;
    }
    rpm = frequency*2;
    time_ms=(double)(micros()-prev_time)/1000;
    theta+=(double)rpm*6*time_ms*multiplier/1000;
    prev_time=micros();
   }   
}
