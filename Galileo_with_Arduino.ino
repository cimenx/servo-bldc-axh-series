
#include <PwmI2c.h>
#include <PID_v1.h>

// pin map
#define VRM       9     //green
//#define SPEED     5     //red
#define BRAKE     7     //white
#define DIRECTION 4     //gray

#define SPEED_ANALOG    A1
#define VRM_ANALOG      A0

#define CW  true
#define CCW false

PID *pid;
double theta, duty_cycle, position;

void setup() {
    Serial.begin(9600);
    Serial1.begin(9600);
    Serial1.setTimeout(100);
    Wire.begin();
    initPwmI2C(VRM, PWM_CLKSRC_1p5M, 1);
//    analogReadResolution(16);

    pinMode(13, OUTPUT);
    pinMode(BRAKE, OUTPUT);
    pinMode(DIRECTION, OUTPUT);
//    pinMode(SPEED, INPUT_FAST);

    fastDigitalWrite(13, HIGH);
    digitalWrite(BRAKE, LOW);
    digitalWrite(DIRECTION, LOW);

    pid = new PID(&theta, &duty_cycle, &position, 0.01, 0, 0, DIRECT);
    pid->SetMode(AUTOMATIC);
    pid->SetOutputLimits(-100,100);
    position = 90;
//    pid->SetSampleTime(sampling_us/500);
}

void loop() {
    unsigned int buf;
    int multiplier;
    while(Serial1.available()){
        Serial1.readBytes((char*)&buf, sizeof(buf));
        if (duty_cycle < 0) multiplier = -1;
        else multiplier = 1;
        theta += (double)buf*multiplier/100;
        if (pid->Compute()) {
            if (duty_cycle < 0) fastDigitalWrite(DIRECTION, CW);
            else fastDigitalWrite(DIRECTION, CCW);
            analogWrite(VRM, map(fabs(duty_cycle), -100, 100, 0, 255));
        }
        Serial.println(theta);
    }
}

