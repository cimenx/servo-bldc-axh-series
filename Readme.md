# Servo Controller Frimware for  BLDC AXH Series
This is code for attempting making controling 
BLDC AXH Series Motor to be like 
Servo Motor using Intel Galileo Gen1. 
But, in our work we found out that 
Intel Galileo Gen1 have limitation 
that it can measure high frequnecy pulse. 

> normal_sampling.ino : calculate pulse frequency use mathematical function based on AXH Driver Datasheet

> pthread_pulse_change.ino : calculate pulse frequnecy by read change of the pulse in separated thread

> period_measure.ino : calculate pulse frequnecy by measures the elapsed time during a single cycle. This works well for relatively low frequencies, because a substantial time elapses. At higher frequencies, the short time can only be measured at the processor's clock speed, which results in limited resolution. 

> pulse_width.ino : calculate pulse frequnecy by counting pulse width