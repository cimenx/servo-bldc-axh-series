#define SPEED 5

unsigned long prev_time, period_us;
unsigned int rpm, theta;
double time_ms;

void setup() {
  Serial.begin(9600);
  pinMode(SPEED, INPUT);
  prev_time = micros();
}

void loop() {
  period_us = pulseIn(SPEED, LOW)+300;
  rpm = (unsigned int)(1000000UL/period_us)*2;
  time_ms = (double)(micros()-prev_time)/1000;
  //theta = (double)rpm*6*time_ms/10;
  if(time_ms<1000){theta=(double)rpm*6*time_ms/10;}
  else {theta=0;}
  Serial.write((byte*)&theta, sizeof(theta));
//  Serial.print((int)theta);
//  Serial.print('\t');
//  Serial.println(time_ms);
  prev_time = micros();
}
