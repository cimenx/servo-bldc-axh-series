
#include <servoAXH.h>
#include <PwmI2c.h>

// pin map
#define VRM       3     //green
#define SPEED     6     //red
#define BRAKE     7     //white
#define DIRECTION 4     //gray

#define SPEED_ANALOG    A1
#define VRM_ANALOG      A0

ServoAXH *mServo;
FreqPeriodCounter *speed;
int fastPin;

volatile double _qEncoderTicks = 0;
volatile unsigned long prev_time = 0;

void setup() {
    Serial.begin(9600);
    //Serial1.begin(9600);
    //Serial1.setTimeout(100);
    Wire.begin();
    initPwmI2C(VRM, PWM_CLKSRC_1p5M, 1);
  //  analogReadResolution(16);

    pinMode(13, OUTPUT);
    pinMode(BRAKE, OUTPUT);
    pinMode(DIRECTION, OUTPUT);
    pinMode(VRM, OUTPUT_FAST);
    pinMode(SPEED, INPUT_FAST);

    fastDigitalWrite(13, HIGH);
    digitalWrite(BRAKE, LOW);
    digitalWrite(DIRECTION, HIGH);
    //fastPin = GPIO_FAST_ID_QUARK_SC(0x40);
//interrupts();
//prev_time = micros();
 attachInterrupt(SPEED, qInterruptHandler, LOW);

speed = new FreqPeriodCounter(SPEED, micros);
   //mServo = new ServoAXH(VRM, SPEED, BRAKE, DIRECTION);
    //mServo->setParam(0.1, 0, 0);
    //mServo->on();
      //         mServo->setPosition(90);
    analogWrite(VRM,50);
}

void loop() {

        //digitalWrite(BRAKE, LOW);
        // mServo->setPosition(90, CW);
        //        Serial.println(map(Serial1.read(),0,255,0,1500));
        //digitalWrite(BRAKE, HIGH);

        //digitalWrite(BRAKE, LOW);
        // mServo->setPosition(180, CCW);
        // sleep(1);
       //  Serial.println("now");
/*
        Serial.print((double)mServo->getSpeed()->elapsedTime/1000000);
        Serial.print('\t');
       Serial.print(mServo->getTheta());
       Serial.print('\t');
        Serial.print(mServo->getVrmDutyCycle());
        Serial.print('\t');
        Serial.print(mServo->getTime_ms());
        Serial.print('\t');
        Serial.print((double)mServo->getSpeed()->period/1000);
        Serial.print('\t');
        Serial.print(mServo->getRpm());
        Serial.print('\n');
        //digitalWrite(BRAKE, HIGH);
        delay(100);
        */
       Serial.println((double)pulseIn2(SPEED, HIGH));

  //_qEncoderTicks = 0;
if(speed->poll()) Serial.println(speed->hertz(1));
  //delay(5);    
}

void qInterruptHandler(){
//_qEncoderTicks++;
  _qEncoderTicks = (micros()-prev_time)/1000;
  prev_time = micros();
}

unsigned long pulseIn2(uint8_t pin, uint8_t state) {

    unsigned long pulseWidth = 0;
    unsigned long loopCount = 0;
    unsigned long loopMax = 5000000;

    // While the pin is *not* in the target state we make sure the timeout hasn't been reached.
    while ((digitalRead(pin)) != state) {
        if (loopCount++ == loopMax) {
            return 0;
        }
    }

    // When the pin *is* in the target state we bump the counter while still keeping track of the timeout.
    while ((digitalRead(pin)) == state) {
        if (loopCount++ == loopMax) {
            return 0;
        }
        pulseWidth++;
    }

    // Return the pulse time in microsecond!
    return pulseWidth * 50; // Calculated the pulseWidth //this is the value to be tweaked
}

