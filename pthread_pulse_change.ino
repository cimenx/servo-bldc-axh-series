#include <TimerOne.h>
 
#define VRM       9     //green
#define SPEED     5     //red
#define BRAKE     7     //white
#define DIRECTION 4     //gray
 
unsigned int count=0;
double f=0;
double sample_ms=0, timeOut = 1;
bool last_state, state;

pthread_t readSpeed_t;
pthread_mutex_t mutexRead;
double mSpeedPeriod;

void *readSpeed(void *arg=NULL) {
  unsigned long prev_time = micros();
  while(1) {
    sample_ms += double(micros()-prev_time)/1000;
    state = fastDigitalRead(SPEED);
    if ((last_state != state) && (state == HIGH))
      count++;
    last_state = state;
    if (sample_ms > timeOut) {
      count=0;
      sample_ms=0;
    }
    prev_time = micros();
  }
}

void threadInitialize(pthread_t &thread, void *(*start_routine)(void *), int priority)
{
  Serial.println("start thread \n");
    int error;
    struct sched_param param;
    pthread_attr_t attr;

    pthread_attr_init(&attr);

    error = pthread_attr_setschedpolicy(&attr, SCHED_RR);
    if(error != 0)
        Serial.println("error = %d\n");

    error = pthread_attr_setinheritsched(&attr, PTHREAD_INHERIT_SCHED);
    if(error != 0)
        Serial.println("error = %d\n");

    memset(&param, 0, sizeof(param));
    param.sched_priority = priority;
    error = pthread_attr_setschedparam(&attr, &param);
    if(error != 0)
        Serial.println("error = %d\n");

    error = pthread_create(&thread, &attr, start_routine, NULL);
    if(error != 0)
        Serial.println("error = %d\n");
}
 
void setup() 
{
  Serial.begin(9600);
  // Initialize the digital pin as an output.
  // Pin 13 has an LED connected on most Arduino boards
  pinMode(13, OUTPUT);    
   pinMode(BRAKE, OUTPUT);
    pinMode(DIRECTION, OUTPUT);
    pinMode(SPEED, INPUT_FAST);
    
    digitalWrite(13, HIGH);
    digitalWrite(BRAKE, LOW);
    digitalWrite(DIRECTION, LOW);
  
//  Timer1.initialize(sample_ms*1000); // set a timer of length 100000 microseconds (or 0.1 sec - or 10Hz => the led will blink 5 times, 5 cycles of on-and-off, per second)
//  Timer1.attachInterrupt( timerIsr ); // attach the service routine here
  threadInitialize(readSpeed_t, readSpeed, 33);
  //Timer2.initialize(100*1000);
  //Timer2.attachInterrupt( Serial.println((double)(count*1000/sample_ms)) );
}
 
    double t=0;
   unsigned long prev_time;
void loop()
{

  // Main code loop
  // TODO: Put your regular (non-ISR) logic here
//  for(int i = 0; i <= 500; i+=2) {
//    while (t < 1) {  
//      t += (double)(millis()-prev_time)/1000;
      Serial.print((double)millis());
      Serial.print('\t');
      Serial.print((double)count*1000/sample_ms);
      Serial.print('\t');
      Serial.println(sample_ms);
      //delay(500);
//      for(long i=0;i<=100000;i++);
//      readSpeed();
//      prev_time=micros();
//    }
//    t=0;
//    sample_ms+=i;
//    Timer1.initialize(sample_ms*1000); // set a timer of length 100000 microseconds (or 0.1 sec - or 10Hz => the led will blink 5 times, 5 cycles of on-and-off, per second)
//    Timer1.attachInterrupt( timerIsr, sample_ms ); // attach the service routine here
//  }
}
 
/// --------------------------
/// Custom ISR Timer Routine
/// --------------------------
void timerIsr()
{
    // Toggle LED
    //noInterrupts();
    //Timer1.stop();
    f = (double)count*1000/sample_ms;
//    Serial.println(f);
    count=0;
    //interrupts();
}


