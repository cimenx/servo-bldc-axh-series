#include <TimerPWM.h>
#include <FreqMeasure.h>
#include <PID_v1.h>
#include <PID_AutoTune_v0.h>

//pin map
#define VRM 5 //green
//#define SPEED 8 //red
#define BRAKE 2 //white
#define DIRECTION 4 //gray

#define CW  true
#define CCW false

PID *pid;
double duty_cycle=0, pos=180;
unsigned int buf;
int multiplier;

double sum=0;
int count=0;
unsigned int sudut=0;
int lastSudut=0;
int rpm=0;
float frequency=0;
double theta, time_ms;
unsigned long prev_time;

byte ATuneModeRemember=2;
double input=80, output=50, setpoint=180;
double kp=2,ki=0.5,kd=2;

double outputStart=5;
double aTuneStep=50, aTuneNoise=1, aTuneStartValue=100;
unsigned int aTuneLookBack=20;

boolean tuning = false;
unsigned long  modelTime, serialTime;

PID_ATune aTune(&theta, &duty_cycle);

//set to false to connect to the real world
boolean useSimulation = false;

void setup()
{
  if(tuning)
  {
    tuning=false;
    changeAutoTune();
    tuning=true;
  }
  
  serialTime = 0;
  Serial.begin(9600);
  
  pinMode(13, OUTPUT);
  pinMode(BRAKE, OUTPUT);
  pinMode(DIRECTION, OUTPUT);
  
  digitalWrite(13, HIGH);
  digitalWrite(BRAKE, LOW);
  digitalWrite(DIRECTION, LOW);
  
  pid = new PID(&theta, &duty_cycle, &pos, kp, ki, kd, DIRECT);
  pid->SetMode(AUTOMATIC);
  pid->SetOutputLimits(-100,100);
  pid->SetSampleTime(1);
  if (pos < 0) multiplier = -1;
  else multiplier = 1;
  
  Tpwm.initialize(0, 4);
  Tpwm.pwm(0, VRM, map(labs(duty_cycle), -100, 100, 0, 1023));
      
  FreqMeasure.begin();
  prev_time=micros();
}

void loop()
{

  unsigned long now = millis();

  if(!useSimulation)
  { //pull the input in from the real world
    readSpeed();
  }
  
  if(tuning)
  {
    byte val = (aTune.Runtime());
    if (val!=0)
    {
      tuning = false;
    }
    if(!tuning)
    { //we're done, set the tuning parameters
      kp = aTune.GetKp();
      ki = aTune.GetKi();
      kd = aTune.GetKd();
      pid->SetTunings(kp,ki,kd);
      AutoTuneHelper(false);
    }
  }
  else {
    controll();
  }
  
//  if(useSimulation)
//  {
//    theta[30]=output;
//    if(now>=modelTime)
//    {
//      modelTime +=100; 
//      DoModel();
//    }
//  }
//  else
//  {
//     analogWrite(0,output); 
//  }
  
  //send-receive with processing if it's time
  if(millis()>serialTime)
  {
    SerialReceive();
    SerialSend();
    serialTime+=500;
  }
}

void changeAutoTune()
{
 if(!tuning)
  {
    //Set the output to the desired starting frequency.
    output=aTuneStartValue;
    aTune.SetNoiseBand(aTuneNoise);
    aTune.SetOutputStep(aTuneStep);
    aTune.SetLookbackSec((int)aTuneLookBack);
    AutoTuneHelper(true);
    tuning = true;
  }
  else
  { //cancel autotune
    aTune.Cancel();
    tuning = false;
    AutoTuneHelper(false);
  }
}

void AutoTuneHelper(boolean start)
{
  if(start)
    ATuneModeRemember = pid->GetMode();
  else
    pid->SetMode(ATuneModeRemember);
}


void SerialSend()
{
  Serial.print("setpoint: ");Serial.print(setpoint); Serial.print(" ");
  Serial.print("input: ");Serial.print(input); Serial.print(" ");
  Serial.print("output: ");Serial.print(output); Serial.print(" ");
  if(tuning){
    Serial.println("tuning mode");
  } else {
    Serial.print("kp: ");Serial.print(pid->GetKp());Serial.print(" ");
    Serial.print("ki: ");Serial.print(pid->GetKi());Serial.print(" ");
    Serial.print("kd: ");Serial.print(pid->GetKd());Serial.println();
  }
}

void SerialReceive()
{
  if(Serial.available())
  {
   char b = Serial.read(); 
   Serial.flush(); 
   if((b=='1' && !tuning) || (b!='1' && tuning))changeAutoTune();
  }
}

void controll()
{
  if (pid->Compute()) {
    if (duty_cycle < 0) {
      digitalWrite(DIRECTION, CW);
      multiplier = -1;
    }
    else {
      digitalWrite(DIRECTION, CCW);
      multiplier = 1;
    }
    Tpwm.setPwmDuty(0, VRM, map(labs(duty_cycle), -100, 100, 0, 1023));
  }
}

void readSpeed()
{
  if (FreqMeasure.available())
  {
    sum = sum + FreqMeasure.read();
    count = count + 1;
    if (count >30)
    {
       frequency = FreqMeasure.countToFrequency(sum/count);
       sum=0;
       count=0;
    }
    rpm = frequency*2;
    time_ms=(double)(micros()-prev_time)/1000;
    theta+=(double)rpm*6*time_ms*multiplier/1000;
    prev_time=micros();
   }   
}

