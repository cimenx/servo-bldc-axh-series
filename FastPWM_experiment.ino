// pin map
#include <Wire.h>
#define VRM       9     //green
#define SPEED     5     //red
#define BRAKE     7     //white
#define DIRECTION 4     //gray
#define readPwm A0

unsigned long time_start = 0, time_end = 0;
#define TIME_S     (time_start = millis())
#define TIME_E     (time_end = millis(), Serial.print("Time:"), Serial.println(time_end - time_start))

int pwm_pin = 9;

pthread_t readSpeed_t;
pthread_mutex_t mutexRead;
double mSpeedPeriod;
int pwmValue=0;
void threadInitialize(pthread_t &thread, void *(*start_routine)(void *), int priority);

void *readSpeed(void *arg) {
    unsigned long prev_time = 0;
    int last_state = fastDigitalRead(SPEED);
    int state;
    while (1) {
        state = fastDigitalRead(SPEED);
        if ((state != last_state) && (state == HIGH)) {
            //            pthread_mutex_lock(&mutexRead);
            prev_time = micros();
            //            pthread_mutex_unlock(&mutexRead);
        }
        else if ((state != last_state) && (state == LOW)) {
            //            pthread_mutex_lock(&mutexRead);
            mSpeedPeriod = (double)(micros()-prev_time)/1000 + 0.3;
            //            pthread_mutex_unlock(&mutexRead);
        }
        last_state = state;
    }
}

///
/// \brief readSpeed
/// \return rpm
///
double readSpeed() {
    static double last_freq = 0;
    double freq=0;
    if (mSpeedPeriod != 0) freq = (1/(mSpeedPeriod/1000));

    // low-pass filter
    freq = freq*0.1 + 0.9*last_freq;
    last_freq = freq;

    return freq*60/30;
}

void analogOut(int pin, double volt) {
    // PWM signal at the default frequency (~600Hz.) / periode
    double rising_time = volt/600/5;
    analogWrite(pin, map(rising_time*100*600, 0, 100, 0, 255));
    pwmValue=analogRead(readPwm);
    //Serial.println(map(rising_time*100*600, 0, 100, 0, 255));
    //Serial.print(" ");
    //Serial.println(pwmValue);
}

void setPosition(double volt, unsigned int delay_ms) {
    analogOut(VRM, volt);
    delayMicroseconds(delay_ms);
    //digitalWrite(BRAKE, HIGH);
}

void setup() {
    Serial.begin(9600);
    Wire.begin();
    pinMode(13, OUTPUT);
    pinMode(BRAKE, OUTPUT);
    pinMode(DIRECTION, OUTPUT);
    pinMode(SPEED, INPUT_FAST);

    digitalWrite(13, HIGH);
    digitalWrite(BRAKE, LOW);
    digitalWrite(DIRECTION, LOW);

    threadInitialize(readSpeed_t, readSpeed, 33);
    //    setSpeed(1000000);
}

void loop() {
    int V = 5;
    int ms = 100000;
//    while (Serial.available()) {
//        V = Serial.parseInt();
//        ms = Serial.parseInt();
//        setPosition(V, ms);
//    }
//    Serial.println(readSpeed());
//    delay(100);
  //setPosition(V,ms);
//  Serial.print(pwmValue);
//  Serial.print('\t');
for (byte i=255;i>0;--i) {
      setPwmI2C(pwm_pin,255);
        delay(250);
  Serial.println(mSpeedPeriod);
  //Serial.print('\t');
  //Serial.println(readSpeed());
}
}

void threadInitialize(pthread_t &thread, void *(*start_routine)(void *), int priority)
{
    int error;
    struct sched_param param;
    pthread_attr_t attr;

    pthread_attr_init(&attr);

    error = pthread_attr_setschedpolicy(&attr, SCHED_RR);
    if(error != 0)
        printf("error = %d\n",error);

    error = pthread_attr_setinheritsched(&attr, PTHREAD_INHERIT_SCHED);
    if(error != 0)
        printf("error = %d\n",error);

    memset(&param, 0, sizeof(param));
    param.sched_priority = priority;
    error = pthread_attr_setschedparam(&attr, &param);
    if(error != 0)
        printf("error = %d\n",error);

    error = pthread_create(&thread, &attr, start_routine, NULL);
    if(error != 0)
        printf("error = %d\n",error);
}

void setPwmI2C(int _iPinNum, int _iPwmVal)
{
  // Select pin to write I2C commands to
  analogWrite(_iPinNum,1);            //analogWrite(pin, 1) will enable the PWM on the pin. 
                                      //but AnalogWrite(pin,0) does not disable the pwm.

  //Then there’s the I2C commands...


  // Select programmable PWM CLK source
  Wire.beginTransmission(0x20);
  Wire.write(0x29);  // CY8C9540A -  Config (29h)
  //xxxxx000b - 32kHz (default)        -> output 100+ HZ
  //xxxxx001b - 24Mhz                  -> output 96.36KHz    
  //xxxxx010b - 1.5Mhz                 -> output 6 kHz
  //xxxxx011b - 93.75 kHz              -> output 378 Hz
  //xxxxx100b - 367.6 Hz (programmable)-> output 367 Hz , can divider CLK
  //xxxxx101b - Previous PWM
  //Wire.write(0x04);  // xxxxx100b - 367.6 Hz (programmable)
  Wire.write(0x02);    // 1.5MHZ , but output just 6khz. just use it~
  Wire.endTransmission();
  
  // Set divider the PWM CLK .
  Wire.beginTransmission(0x20);
  Wire.write(0x2C);
  Wire.write(1);
  Wire.endTransmission();

  // Set period register (2Ah) - 0x01 / 0xff
  Wire.beginTransmission(0x20);
  Wire.write(0x2a);
  //xxxx0xxxb - Falling pulse edge (default)      
  //xxxx1xxxb - Rising pulse edge
  Wire.write(0xff); // Selete Rising pulse edge.
  Wire.endTransmission();

  // Set minimum duty cycle - Pulse Width Register (2Bh)
  Wire.beginTransmission(0x20);
  Wire.write(0x2b);
  //This register sets the pulse width of the PWM output. 
  //Allowed values are between zero and the (Period - 1) value.
  ////// this is the pulse width...0-255 where 255 is all "on" for one cycle (1/clock frequency)
  Wire.write(_iPwmVal);
  Wire.endTransmission();
}
