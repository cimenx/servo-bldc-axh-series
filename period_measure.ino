#include <MsTimer2.h>
#include <TimerPWM.h>
#include <FreqMeasure.h>

//pin map
#define VRM 5 //green
#define SPEED 8 //red
#define BRAKE 2 //white
#define DIRECTION 4 //gray

#define CW  true
#define CCW false

int duty_cycle=0;

double sum=0;
int count=0;
volatile int rpm=0,rpmbuf[200];
float frequency=0;

volatile unsigned long x,y; // x = freq, y = jumlah data
//unsigned long timestamp,start;
void setup() {
  
  pinMode(13, OUTPUT);
  pinMode(BRAKE, OUTPUT);
  pinMode(DIRECTION, OUTPUT);
  
  digitalWrite(13, HIGH);
  digitalWrite(BRAKE, LOW);
  digitalWrite(DIRECTION, LOW);
  
//  Tpwm.initialize(0, 100);
//  Tpwm.pwm(0, VRM, 128);
  FreqMeasure.begin();
    Serial.begin(9600);
   MsTimer2::set(5, counting); // 5ms period
//      Tpwm.initialize(0, 3);

}

void loop() {
  for(y=0;y<200;y++) readSpeed();
  for (duty_cycle=25;duty_cycle<=100;duty_cycle+=25) {
    for (x=3;x<=2003;x+=100)
    {
      Tpwm.initialize(0, x);
      Tpwm.pwm(0, VRM, map(abs(duty_cycle), 0, 100, 0, 1023));
      y=0;rpm=0;
      MsTimer2::start();
      while(y<200)
      {
//         delay(1000);
         readSpeed();
//         delay(1000);
//         digitalWrite(13,HIGH);
//         delay(100);
//         digitalWrite(13,LOW);
//         delay(50);
      }
      MsTimer2::stop();
      Tpwm.stop(0);
      digitalWrite(BRAKE, HIGH);
      for(y=0;y<200;y++) 
      {
       Serial.print(duty_cycle);
       Serial.print('\t');
       Serial.print(x);
       Serial.print('\t');
       Serial.println(rpmbuf[y]);
      }
      digitalWrite(BRAKE, LOW);
      for(y=0;y<200;y++) {
        readSpeed();
        rpmbuf[y] = 0;
      }
    } 
  }
}

void counting()
{
  rpmbuf[y]=rpm;
  y++;
}

void readSpeed()
{
  if (FreqMeasure.available())
  {
    rpm = FreqMeasure.countToFrequency(FreqMeasure.read()/1);
//    sum = sum + FreqMeasure.read();
//    count = count + 1;
//    if (count >10)
//    {
//       frequency = FreqMeasure.countToFrequency(sum/count);
//       sum=0;
//       count=0;
//       rpm = frequency*2;
//    }
  }   
}

