#ifndef SERVOAXH_H
#define SERVOAXH_H

#include <pthread.h>
#include <PID_v1.h>
#include <HardwareSerial.h>
#include <FreqPeriodCounter.h>

#define CW  true
#define CCW false

class ServoAXH
{
public:
    ServoAXH(int vrmPin, int speedPin, int brakePin, int directionPin, int startPin=-1);
    ServoAXH(int vrmPin, HardwareSerial *speedData, int brakePin, int directionPin, int startPin=-1);
    ~ServoAXH();

    void on();
    void off();

    void setParam(double Kp, double Ki, double Kd);
    void setPosition(double pos);

    double getTheta() const;
    void setTheta(double value);

    double getTime_ms() const;
    double getVrmDutyCycle() const;
    double getRpm() const;

    FreqPeriodCounter *getSpeed() const;

private:
    int vrmPin;
    int speedPin;
    HardwareSerial *speedSerial;
    int brakePin;
    int directionPin;
    int startPin=-1;

    bool isOn;
    bool isRun;
    bool isSerial;
    bool dir;

    double rpm;
    double vrmDutyCycle;
    double position;
    double theta;

    double time_ms;
    unsigned long prev_time;

    PID *pid;
    FreqPeriodCounter *speed;
    pthread_t run_t;

    static void *run(void *arg);
};

#endif // SERVOAXH_H
