#include "servoAXH.h"
#include <Arduino.h>

ServoAXH::ServoAXH(int vrmPin, int speedPin,
                   int brakePin, int directionPin, int startPin):
    vrmPin(vrmPin), speedPin(speedPin),
    brakePin(brakePin), directionPin(directionPin), startPin(startPin),
    isOn(false), isRun(false), isSerial(false)
{

}

ServoAXH::ServoAXH(int vrmPin, HardwareSerial *speedData,
                   int brakePin, int directionPin, int startPin):
    vrmPin(vrmPin), speedSerial(speedData),
    brakePin(brakePin), directionPin(directionPin), startPin(startPin),
    isOn(false), isRun(false), isSerial(true)
{

}

ServoAXH::~ServoAXH()
{

}

void ServoAXH::on()
{
    if (! isOn) {
        int error;
        struct sched_param param;
        pthread_attr_t attr;

        pthread_attr_init(&attr);

        error = pthread_attr_setschedpolicy(&attr, SCHED_RR);
        if(error != 0)
            printf("error = %d\n",error);

        error = pthread_attr_setinheritsched(&attr, PTHREAD_INHERIT_SCHED);
        if(error != 0)
            printf("error = %d\n",error);

        memset(&param, 0, sizeof(param));
        param.sched_priority = 33;
        error = pthread_attr_setschedparam(&attr, &param);
        if(error != 0)
            printf("error = %d\n",error);

        error = pthread_create(&run_t, &attr, run, this);
        if(error != 0)
            printf("error = %d\n",error);
        isOn = (error == 0);
    }
}

void ServoAXH::off()
{
    if (isOn) {
        isOn = false;
        delay(500);
        pthread_exit(&run_t);
    }
}

void ServoAXH::setParam(double Kp, double Ki, double Kd)
{
    pid = new PID(&theta, &vrmDutyCycle, &position, Kp, Ki, Kd, DIRECT);
    pid->SetMode(AUTOMATIC);
    pid->SetOutputLimits(-100,100);
    pid->SetSampleTime(50);
    if (!isSerial) speed = new FreqPeriodCounter(speedPin, micros, 300);
}

void ServoAXH::setPosition(double pos)
{
    position = pos;
}

double ServoAXH::getTheta() const
{
    return theta;
}

double ServoAXH::getTime_ms() const
{
    return time_ms;
}

double ServoAXH::getVrmDutyCycle() const
{
    return vrmDutyCycle;
}

double ServoAXH::getRpm() const
{
    return rpm;
}
FreqPeriodCounter *ServoAXH::getSpeed() const
{
    return speed;
}

void *ServoAXH::run(void *arg)
{
    ServoAXH *servo = (ServoAXH *)arg;
    double last_rpm[10];
    for (int b = 0; b < 10; ++b) {
        last_rpm[b]=b;
    }
    short int i = 10;
    servo->rpm = 0;
    bool data_ok=false;
    servo->vrmDutyCycle = 0;
    //    bool direction = servo->dir;
    servo->prev_time = micros();
    while (servo->isOn) {
        servo->time_ms = (double)(micros()-servo->prev_time)/1000;
        servo->theta += servo->rpm*6*servo->time_ms/1000;
        servo->prev_time = micros();

        if (servo->pid->Compute()) {
            if (servo->vrmDutyCycle < 0) fastDigitalWrite(servo->directionPin, CW);
            else fastDigitalWrite(servo->directionPin, CCW);
            analogWrite(servo->vrmPin, map(fabs(servo->vrmDutyCycle), 0, 100, 0, 255));
        }

        if (servo->isSerial) {
            if (servo->speedSerial->available())
                servo->rpm = map(servo->speedSerial->read(), 0, 255, 0, 1500)*60/30;
            else servo->rpm = 0;
        }
		//ngukur pulsa
        else if(servo->speed->poll()) {
            data_ok = false;
            for (int a = 0; a <= 10; ++a)
                data_ok |= (servo->rpm != last_rpm[a]);
            if (1) servo->rpm = (servo->speed->hertz(1)*60/30);
            else servo->rpm = 0;
//speed
            if (servo->rpm > 3000) servo->rpm = 3000;
//            servo->rpm = servo->rpm*0.5 + 0.5*last_rpm[i];
            if (servo->vrmDutyCycle < 0) servo->rpm *= -1;

            if (i < 0) i = 10;
            last_rpm[i] = servo->rpm;
            i--;
        }
    }
}