#ifndef PWMI2C_H
#define PWMI2C_H
#include <Arduino.h>
#include <Wire.h>

#define PWM_CLKSRC_32K      0b000   // 32kHz (default) -> output 100+ HZ
#define PWM_CLKSRC_24M      0b001   // 24Mhz -> output 96.36KHz
#define PWM_CLKSRC_1p5M     0b010   // 1.5Mhz -> output 6 kHz
#define PWM_CLKSRC_93K      0b011   // 93.75 kHz -> output 378 Hz
#define PWM_CLKSRC_367      0b100   // 367.6 Hz (programmable)-> output 367 Hz , can divider CLK
#define PWM_CLKSRC_PREVIOUS 0b101   // Previous PWM

///
/// \brief setPwmI2C
/// set PWM use I2C. Need 55ms
/// \param _iPinNum
/// \param _iClkSrc
/// \param _iDiv
///
void initPwmI2C(int _iPinNum, byte _iClkSrc, byte _iDiv);

///
/// \brief setPwm
/// just set pwm period & pulse of the PWM output. Need 10ms
/// \param _iPinNum
/// \param _iPwmVal
///
void setPwmI2C(int _iPinNum, int _iPwmVal);

#endif // PWMI2C_H
