#include "PwmI2c.h"

void initPwmI2C(int _iPinNum, byte _iClkSrc, byte _iDiv)
{
    // Select pin to write I2C commands to
    analogWrite(_iPinNum,1); //analogWrite(pin, 1) will enable the PWM on the pin.
    //but AnalogWrite(pin,0) does not disable the pwm.
    //Then there’s the I2C commands...
    // Select programmable PWM CLK source
    Wire.beginTransmission(0x20);
    Wire.write(0x29); // CY8C9540A - Config (29h)
    //xxxxx000b - 32kHz (default) -> output 100+ HZ
    //xxxxx001b - 24Mhz -> output 96.36KHz
    //xxxxx010b - 1.5Mhz -> output 6 kHz
    //xxxxx011b - 93.75 kHz -> output 378 Hz
    //xxxxx100b - 367.6 Hz (programmable)-> output 367 Hz , can divider CLK
    //xxxxx101b - Previous PWM
    //Wire.write(0x04); // xxxxx100b - 367.6 Hz (programmable)
    Wire.write(_iClkSrc); // 1.5MHZ , but output just 6khz. just use it~
    Wire.endTransmission();
    // Set divider the PWM CLK .
    Wire.beginTransmission(0x20);
    Wire.write(0x2C);
    Wire.write(_iDiv);
    Wire.endTransmission();
}

void setPwmI2C(int _iPinNum, int _iPwmVal)
{
    if(_iPwmVal > 250) _iPwmVal = 250;
    // Select pin to write I2C commands to
    analogWrite(_iPinNum,1); //analogWrite(pin, 1) will enable the PWM on the pin.
    // Set period register (2Ah) - 0x01 / 0xff
    Wire.beginTransmission(0x20);
    Wire.write(0x2a);
    //xxxx0xxxb - Falling pulse edge (default)
    //xxxx1xxxb - Rising pulse edge
    Wire.write(0xff); // Selete Rising pulse edge.
    Wire.endTransmission();
    // Set minimum duty cycle - Pulse Width Register (2Bh)
    Wire.beginTransmission(0x20);
    Wire.write(0x2b);
    //This register sets the pulse width of the PWM output.
    //Allowed values are between zero and the (Period - 1) value.
    ////// this is the pulse width...0-255 where 255 is all "on" for one cycle (1/clock frequency)
    Wire.write(_iPwmVal);
    Wire.endTransmission();
}

